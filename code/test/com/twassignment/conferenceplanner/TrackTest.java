package com.twassignment.conferenceplanner;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class TrackTest {

	static Track trackWithMorningSession = new Track();
	static Track trackWithOutMorningSession = new Track();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		// data to test isMorningOccupied
		Session session = new Session();
		session.becomeMorningSession();
		trackWithMorningSession.addSession(session);
		
		Session session2 = new Session();
		session2.becomeLunchSession();
		trackWithOutMorningSession.addSession(session2);
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsMorningOccupied() {
		assertEquals(true, trackWithMorningSession.isMorningOccupied());
		assertEquals(false, trackWithOutMorningSession.isMorningOccupied());
	}

	@Ignore
	public void testAddLunchSession() {
		Track track = new Track();
		track.addLunchSession();
		boolean foundLunchSession = false;
		for(Session session : track.getSessions()){
			if(session.isLunchSession()){
				foundLunchSession = true;
			}
		}
		assertEquals(true, foundLunchSession);
	}

	@Ignore
	public void testAddNetworkEvent() {
		Track track = new Track();
		Session session = new Session();
		session.becomeAfternoonSession();
		track.addNetworkEvent();
		boolean foundNWEvent = false;
		for(Session tempSession : track.getSessions()){
			if(tempSession.isAfterNoonSession()){
				for(Event tempEvent : tempSession.getEvents()){
					if(tempEvent instanceof NetworkEvent){
						foundNWEvent = true;
					}
				}
			}
		}
		assertEquals(true, foundNWEvent);
	
	
	}

}
