/**
 * 
 */
package com.twassignment.conferenceplanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.twassignment.conferenceplanner.common.AppConfPlanner;

/**
 * @author manohar
 *
 */
public class PlannerTest {

	
	static List<Event> firstSetOfEvents = new ArrayList<Event>();
	static List<Event> secondSetOfEvents = new ArrayList<Event>();
	static List<Event> unsortedListOfEvents = new ArrayList<Event>();
	static List<Event> List1ToTestFindSubset = new ArrayList<Event>();
	static List<Event> List2ToTestFindSubset = new ArrayList<Event>();
	static List<Event> List3ToTestFindSubset = new ArrayList<Event>();
	static List<Event> List4ToTestFindSubset = new ArrayList<Event>();
	static List<Event> List1ToTestNoOfSessions = new ArrayList<Event>();
	static List<Event> List1ToTestNoOfTracks = new ArrayList<Event>();
	
/*	static List<Event> eventsToTestNoOfTracks = new ArrayList<Event>();*/
	Planner planner = new Planner();
	
	@BeforeClass
	public static void testSetup(){
		for(int i = 0 ; i < 5 ; i++){
			Event event = new Talk();
			event.setEventId(i);
			event.setDuration(30*i);
			firstSetOfEvents.add(event);
		}
		
		for(int i = 0 ; i < 5 ; i++){
			Event event = new Talk();
			event.setEventId(i);
			event.setDuration(60*i);
			secondSetOfEvents.add(event);
		}
		
		Random random = new Random();
		for(int i = 0 ; i<5 ; i++){
			Event event = new Talk();
			event.setDuration(i*random.nextInt(30));
			unsortedListOfEvents.add(event);
		}
		
		List1ToTestFindSubset.add(new Talk(60));
		List1ToTestFindSubset.add(new Talk(45));
		List1ToTestFindSubset.add(new Talk(30));
		List1ToTestFindSubset.add(new Talk(45));
		List1ToTestFindSubset.add(new Talk(45));
		List1ToTestFindSubset.add(new Talk(05));
		
		List2ToTestFindSubset.add(new Talk(60));
		List2ToTestFindSubset.add(new Talk(45));
		List2ToTestFindSubset.add(new Talk(30));
		List2ToTestFindSubset.add(new Talk(30));
		List2ToTestFindSubset.add(new Talk(45));
		List2ToTestFindSubset.add(new Talk(60));
		
		List4ToTestFindSubset.add(new Talk(30));
		List4ToTestFindSubset.add(new Talk(30));
		
		
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		List1ToTestNoOfSessions.add(new Talk(179));
		
		
		List1ToTestNoOfTracks.add(new Talk(60));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(05));
		List1ToTestNoOfTracks.add(new Talk(60));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(60));
		List1ToTestNoOfTracks.add(new Talk(60));
		List1ToTestNoOfTracks.add(new Talk(45));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(60));
		List1ToTestNoOfTracks.add(new Talk(30));
		List1ToTestNoOfTracks.add(new Talk(30));
		
		
		
	}
	
	/**
	 * @throws java.lang.Exception
	 *//*
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}*/

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.twassignment.conferenceplanner.Planner#planConference(java.util.List)}.
	 */
	@Test
	public void testPlanConference() {
		List<Event> inputEvent = new ArrayList<Event>();
		inputEvent.addAll(List1ToTestNoOfTracks);
		List<Track> tracks = new ArrayList<>();
		tracks.addAll(planner.planConference(inputEvent));
		assertEquals(2, tracks.size());
		int totalSessions = 0;
		for(Track track : tracks){
			totalSessions += track.getSessions().size();
		}
		assertEquals(6, totalSessions);
	}

	/**
	 * Test method for {@link com.twassignment.conferenceplanner.Planner#sortEvents(java.util.List)}.
	 */
	@Test
	public void testSortEvents() {
		AppConfPlanner.sortEvents(unsortedListOfEvents);
		for (int i = 0; i < 5 - 1; i++) {
			assertEquals(
					true,
					unsortedListOfEvents.get(i).getDuration() >= unsortedListOfEvents
							.get(i + 1).getDuration());
		}
	}

	/**
	 * Test method for {@link com.twassignment.conferenceplanner.Planner#calculateNoOfTracks(java.util.List)}.
	 */
	@Ignore
	public void testCalculateNoOfTracks() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.twassignment.conferenceplanner.Planner#groupInToSessions(java.util.List, int)}.
	 */
	@Ignore
	public void testGroupInToSessions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.twassignment.conferenceplanner.Planner#findSubSetOfEvents(java.util.List, int)}.
	 */
	@Test
	public void testFindSubSetOfEvents() {
		List<Event> subsetEvents = planner.findSubSetOfEvents(List1ToTestFindSubset, 180);
		assertEquals(180, AppConfPlanner.sumEvents(subsetEvents));
		
		subsetEvents.clear();
		subsetEvents = planner.findSubSetOfEvents(List2ToTestFindSubset, 180);
		assertEquals(180, AppConfPlanner.sumEvents(subsetEvents));
		
		/*subsetEvents.clear();
		subsetEvents = planner.findSubSetOfEvents(List3ToTestFindSubset, 180);
		assertEquals(AppConfPlanner.sumEvents(List3ToTestFindSubset), AppConfPlanner.sumEvents(subsetEvents));*/
		
		subsetEvents.clear();
		subsetEvents = planner.findSubSetOfEvents(List4ToTestFindSubset, 180);
		assertEquals(60, AppConfPlanner.sumEvents(subsetEvents));
	}
	
	@Test
	public void testNoOfSessionFormedIsProper(){
		List<Event> inputEvent = new ArrayList<Event>();
		inputEvent.addAll(List1ToTestNoOfSessions);
		List<Track> tracks = planner.planConference(inputEvent);
		int totalSessionCreated = 0;
		for(Track track : tracks){
			for(Session session : track.getSessions()){
				if(!session.isLunchSession()){
					totalSessionCreated += 1;
				}
			}
		}
		assertEquals(11, totalSessionCreated);
	}

	@Test
	public void testNoOfTracks(){
		List<Event> inputEvent = new ArrayList<Event>();
		inputEvent.addAll(List1ToTestNoOfTracks);
		List<Track> tracks = planner.planConference(inputEvent);
		assertEquals(2, tracks.size());
		int totalSessions = 0;
		for(Track track : tracks){
			totalSessions += track.getSessions().size();
		}
		assertEquals(6, totalSessions);
	}
	
}
