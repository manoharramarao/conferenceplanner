package com.twassignment.conferenceplanner;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class EventTest {

	static List<Event> firstSetOfEvents = new ArrayList<Event>();
	static List<Event> secondSetOfEvents = new ArrayList<Event>();
	
	@BeforeClass
	public static void testSetup(){
		for(int i = 0 ; i < 5 ; i++){
			Event event = new Talk();
			event.setEventId(i);
			event.setDuration(30*i);
			firstSetOfEvents.add(event);
		}
		
		for(int i = 0 ; i < 5 ; i++){
			Event event = new Talk();
			event.setEventId(i);
			event.setDuration(60*i);
			secondSetOfEvents.add(event);
		}
	}
	
	@AfterClass
	public static void testCleanup(){
		
	}
	
	@Test
	public void testEqualsObject() {
		
		for(int i = 0 ; i < 5 ; i++){
			assertEquals(firstSetOfEvents.get(i).equals(secondSetOfEvents.get(i)), true);
		}
	}

}
