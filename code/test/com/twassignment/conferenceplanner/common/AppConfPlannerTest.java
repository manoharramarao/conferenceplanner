package com.twassignment.conferenceplanner.common;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.twassignment.conferenceplanner.Event;
import com.twassignment.conferenceplanner.Talk;

public class AppConfPlannerTest {
	
	static List<Event> eventsToTestSum = new ArrayList<Event>();
	static int eventsDurationSum = 0;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Random random = new Random();
		for(int i = 0 ; i<5 ; i++){
			Event event = new Talk();
			int num = i*random.nextInt(30);
			event.setDuration(num);
			eventsToTestSum.add(event);
			eventsDurationSum += num;
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSumEvents() {
		assertEquals(eventsDurationSum, AppConfPlanner.sumEvents(eventsToTestSum));
	}

}
