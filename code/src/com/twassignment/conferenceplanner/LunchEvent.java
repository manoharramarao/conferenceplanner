package com.twassignment.conferenceplanner;

import com.twassignment.conferenceplanner.common.ConfConstants;
import com.twassignment.conferenceplanner.common.Utility;

/**
 * Kind of Event and its name is Lunch. Default duration is 60 mins.
 * 
 * @see Event
 * @author manohar
 * @version 1.0
 */
public class LunchEvent extends Event {

	public LunchEvent() {
		super();
	}
	
	public LunchEvent(int id, int duration){
		this.setEventId(id);
		this.setDuration(duration);
	}
	
	/**
	 * @see Event#setName(String)
	 * @see ConfConstants#LUNCH_EVENT_NAME
	 */
	@Override
	public void setName(String name) {
		if (Utility.isEmpty(name)) {
			name = ConfConstants.LUNCH_EVENT_NAME;
		}
		super.setName(name);
	}

	/**
	 * @see Event#setDescription(String)
	 * @see ConfConstants#LUNCH_EVENT_DESCRIPTION
	 */
	@Override
	public void setDescription(String description) {
		if (Utility.isEmpty(description)) {
			description = ConfConstants.LUNCH_EVENT_DESCRIPTION;
		}
		super.setDescription(description);
	}

	/**
	 * @see Event#setEndTime(String)
	 * @see ConfConstants#LUNCH_EVENT_END_TIME
	 */
	@Override
	public void setEndTime(String endTime) {
		if (Utility.isEmpty(endTime)) {
			endTime = ConfConstants.LUNCH_EVENT_END_TIME;
		}
		super.setEndTime(endTime);
	}

	/**
	 * @see Event#setStartTime(String)
	 * @see ConfConstants#LUNCH_EVENT_START_TIME
	 */
	@Override
	public void setStartTime(String startTime) {
		if (Utility.isEmpty(startTime)) {
			startTime = ConfConstants.LUNCH_EVENT_START_TIME;
		}
		super.setStartTime(startTime);
	}
}
