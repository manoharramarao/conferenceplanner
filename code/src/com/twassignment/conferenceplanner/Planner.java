package com.twassignment.conferenceplanner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;

import com.twassignment.conferenceplanner.common.AppConfPlanner;
import com.twassignment.conferenceplanner.common.ConfConstants;
import com.twassignment.conferenceplanner.common.EventComparatorByDuration;
import com.twassignment.conferenceplanner.common.Utility;

public class Planner {
	
	List<Track> tracks = new ArrayList<Track>();
	List<Session> sessions = new ArrayList<Session>();
	List<Event> masterEventList = new ArrayList<Event>();
	
	List<Event> eventSubset = new ArrayList<Event>();
	List<Event> temp1Subset = new ArrayList<Event>();
	List<Event> temp2Subset = new ArrayList<Event>();
	int count = 0;
	
	
	/**
	 * Plans the conference taking list of events. This is the mother method for
	 * this project :)
	 * 
	 * @param events
	 */
	public List<Track> planConference(List<Event> events){

		List<Event> ignoreEvents = new ArrayList<Event>();
		if (events != null && !events.isEmpty()) {
			for (Iterator<Event> itr = events.iterator(); itr.hasNext();) {
				Event tempEvent = itr.next();
				if (tempEvent.getDuration() == 180
						|| (tempEvent.getDuration() > ConfConstants.SESSION_DURATION && tempEvent
								.getDuration() <= (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER))) {
					Session session = new Session();
					session.addEvents(tempEvent);
					sessions.add(session);
					itr.remove();
				}// ignore events that have duration > after noon session duration
				else if (tempEvent.getDuration() > (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER)) {
					Utility.getLogger().log(Level.WARNING, "event ignored => " + tempEvent.toString());
					itr.remove();
				}
			}
			// after doing initial changes if events still exist then go for identifying subset
			if(events != null && !events.isEmpty()){
				groupInToSessions(events, ConfConstants.SESSION_DURATION);
			}
		} else {
			Utility.getLogger().log(Level.SEVERE, "no proper events given as input. Try to give events whose duration < 240");
		}
		
		if(sessions != null && !sessions.isEmpty()){
			
			List<Session> incompleteSessions = new ArrayList<Session>();
			List<Session> perfectlyTimedSessions = new ArrayList<Session>();
			List<Session> perfectSessions = new ArrayList<Session>();
			
			incompleteSessions.addAll(populateIncompleteSessions(sessions));
			perfectlyTimedSessions.addAll(populatePerfectlyTimedSessions(sessions));
			perfectSessions.addAll(removeHalfOfPerfectlyTimedSessions(perfectlyTimedSessions));

			// Asjust incomplete sessions with the afternoon sessions if they have some time
			while (!incompleteSessions.isEmpty() && !perfectSessions.isEmpty()) {

				Session firstIncSession = incompleteSessions.get(0);
				incompleteSessions.remove(firstIncSession);
				sessions.remove(firstIncSession);

				Session firstPerSession = perfectSessions.get(0);
				perfectSessions.remove(0);
				sessions.remove(firstPerSession);

				List<Event> incEvents = new ArrayList<Event>();
				List<Event> perfEvents = new ArrayList<Event>();

				incEvents.addAll(firstIncSession.getEvents());
				perfEvents.addAll(firstPerSession.getEvents());

				if (incEvents != null && !incEvents.isEmpty()) {
					for (Iterator<Event> itr = incEvents.iterator(); itr.hasNext();) {
						Event incEvent = itr.next();
						if ((AppConfPlanner.sumEvents(perfEvents) + incEvent.getDuration()) <= (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER)) {
							perfEvents.add(incEvent);
							itr.remove();
						}
					}
				}
				firstIncSession.setEvents(incEvents);
				firstPerSession.setEvents(perfEvents);

				sessions.add(firstPerSession);
				if (firstIncSession.getEvents() != null
						&& !firstIncSession.getEvents().isEmpty()) {
					sessions.add(firstIncSession);
				}

				incompleteSessions.clear();
				perfectlyTimedSessions.clear();
				incompleteSessions.addAll(populateIncompleteSessions(sessions));
			}
			
			// merge 2 incomplete sessions
			if(!incompleteSessions.isEmpty() && perfectSessions.isEmpty() && incompleteSessions.size() >= 2){
				
				for(int i = 0; i < (incompleteSessions.size()/2) ; i++){
					Session firstIncSession = incompleteSessions.get(i);
					Session secondIncSession = incompleteSessions.get(i+1);
					sessions.remove(firstIncSession);
					sessions.remove(secondIncSession);
					
					List<Event> firstIncEvents = new ArrayList<Event>();
					List<Event> secondIncEvents = new ArrayList<Event>();
					
					firstIncEvents.addAll(firstIncSession.getEvents());
					secondIncEvents.addAll(secondIncSession.getEvents());
					
					for(Iterator<Event> itr = secondIncEvents.iterator(); itr.hasNext();){
						Event tempEvent = itr.next();
						if (AppConfPlanner.sumEvents(firstIncEvents)
								+ tempEvent.getDuration() < (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER)) {
							firstIncEvents.add(tempEvent);
							itr.remove();
						}
					}
					firstIncSession.setEvents(firstIncEvents);
					secondIncSession.setEvents(secondIncEvents);
					sessions.add(firstIncSession);
					if(secondIncSession.getEvents() != null 
							&& !secondIncSession.getEvents().isEmpty()){
						sessions.add(secondIncSession);
					}
					incompleteSessions.clear();
					incompleteSessions.addAll(populateIncompleteSessions(sessions));
				}
				
			}
			
			// assign sessions to tracks
			createTracksAndAssignSessions(sessions);
			
			// add lunch and networking events
			addLunchAndNetworkingEvents(tracks);
			
			// assign start time end time to events inside tracks
			addStartAndEndTimes(tracks);
		}
		return tracks;
	}

	// TODO make it private
	/*public int calculateNoOfTracks(List<Event> events){
		int totalDuration = 0;
		for(Event event : events){
			totalDuration += event.getDuration();
		}
		Utility.getLogger().log(Level.FINEST, "sum duration of events = " + totalDuration);
		return (totalDuration / ConfConstants.SESSION_DURATION) / 2;
	}*/
	
	/**
	 * 
	 * @param events
	 * @param totalDuration
	 * @return
	 */
	public List<Session> groupInToSessions(List<Event> events, int totalDurationOfASession){
		
		count++;
		if(count == 1){
			masterEventList.addAll(events);
		}
		eventSubset.clear();
		temp1Subset.clear();
		temp2Subset.clear();
		if(events == null || events.isEmpty()){
			return sessions;
		}else{
			List<Event> subSetEvents = findSubSetOfEvents(events,
					totalDurationOfASession);
			if(eventSubset != null && !eventSubset.isEmpty()){
				Session session = new Session();
				session.addEvents(eventSubset);
				sessions.add(session);
				masterEventList.removeAll(eventSubset);
			}else if(eventSubset == null || eventSubset.isEmpty()){
				for(Iterator<Event> itr = masterEventList.iterator() ; itr.hasNext();){
					Event tempEvent = itr.next();
					Session session = new Session();
					session.addEvents(tempEvent);
					sessions.add(session);
					itr.remove();
				}
			}
			events.clear();
			if(masterEventList != null && !masterEventList.isEmpty()){
				events.addAll(masterEventList);
			}
			return groupInToSessions(events, totalDurationOfASession);
		}
	}
	
	/**
	 * Iterates through the event and inserts event into session if it can
	 * accommodate it. Not used as of now
	 * 
	 * @param events
	 */
	private List<Session> adjustEventsToSession(List<Event> events, List<Session> sessionsList) {
		/* List<Event> adjustedEventList = new ArrayList<>(); */
		int totalDurationLeftInIncompleteSessions = 0;
		if (sessionsList != null && !sessionsList.isEmpty()) {
			for (Session tempSession : sessionsList) {
				totalDurationLeftInIncompleteSessions += ((ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER) - tempSession.getDuration());
			}
			if (AppConfPlanner.sumEvents(events) < totalDurationLeftInIncompleteSessions) {
				List<Event> eventsToIterate = new ArrayList<>();
				eventsToIterate.addAll(events);
				// TODO have null check
				for (Event tempEvent : eventsToIterate) {
					List<Session> sessionsToIterate = new ArrayList<>();
					sessionsToIterate.addAll(sessionsList);
					for (Session tempSession : sessionsToIterate) {
						if (doesEventFitsInAfternoonSession(tempEvent, tempSession)) {
							// remove from incomplete sessions list
							sessionsList.remove(tempSession);

							// remove from parent events list
							events.remove(tempEvent);

							// modify the session by setting new event list
							List<Event> tempListEvents = tempSession
									.getEvents();
							tempListEvents.add(tempEvent);
							tempSession.addEvents(tempListEvents);

							// set the modified session back to
							// incompletesessions list
							sessionsList.add(tempSession);
							break;
						}
					}
				}
			}
		}
		return sessionsList;
	}
	
	
	/**
	 * Find subset of events whose sum matches the total session duration.
	 * session duration is given by {@link ConfConstants#SESSION_DURATION} 
	 * @see ConfConstants#SESSION_DURATION
	 *  
	 * @param events
	 * @param minsLeft
	 * @return
	 */
	public List<Event> findSubSetOfEvents(List<Event> events, int minsLeft) {
		
		if (events != null && !events.isEmpty()) {
			AppConfPlanner.sortEvents(events);
		}

		if (minsLeft > 0) {
			if (events.isEmpty()) {
				boolean foundInTempStack = false;
				if (temp1Subset != null && !temp1Subset.isEmpty()) {
					for (Iterator<Event> itr = temp1Subset.iterator(); itr.hasNext();) {
						Event eventFromTemp1Stack = itr.next();
						if (eventFromTemp1Stack.getDuration() == minsLeft) {
							eventSubset.add(eventFromTemp1Stack);
							itr.remove();
							foundInTempStack = true;
							break;
						} else {
							itr.remove();
							events.add(eventFromTemp1Stack);
						}
					}
				} else if (!foundInTempStack
						&& (temp2Subset != null && !temp2Subset.isEmpty())) {
					for (Iterator<Event> itr = temp2Subset.iterator(); itr
							.hasNext();) {
						Event eventFromTemp2Stack = itr.next();
						if (eventFromTemp2Stack.getDuration() == minsLeft) {
							eventSubset.add(eventFromTemp2Stack);
							itr.remove();
							foundInTempStack = true;
							break;
						} else {
							itr.remove();
							events.add(eventFromTemp2Stack);
						}
					}
				}
				
				// This if condition is to satify scenarios like [20,40,25]
				if (((events == null || events.isEmpty())
						&& (temp1Subset == null || temp1Subset.isEmpty()) && (temp2Subset == null || temp2Subset
						.isEmpty()))) {
					if (AppConfPlanner.sumEvents(eventSubset) <= (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER)) {
						return findSubSetOfEvents(events, 0);
					}
				} /*else if (AppConfPlanner.sumEvents(events) // This else if condition is completely independent of subset of 180 logic.
						+ AppConfPlanner.sumEvents(eventSubset) // this condition is to satisfy the scenario, when you have only events that are sufficient
						+ AppConfPlanner.sumEvents(temp1Subset)// for 1 afternoon session and no other session (morning or afternoon). 60,45,30,30,30,60,30,30
						+ AppConfPlanner.sumEvents(temp2Subset) < (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER)) {
					eventSubset.addAll(events);
					eventSubset.addAll(temp1Subset);
					eventSubset.addAll(temp2Subset);
					return findSubSetOfEvents(events, 0);
				}*/else if (!foundInTempStack && (eventSubset.size() > 2)) {
					int end = eventSubset.size() - 1;
					int begin = 0;
					for (; end > 1; end--) {
						events.add(eventSubset.remove(end));
						/* eventSubset.remove(end); */
					}
					eventSubset.remove(end);
				}else if(!foundInTempStack && (eventSubset.size() <= 2) && !events.isEmpty()){
					eventSubset.clear();
					return findSubSetOfEvents(events, ConfConstants.SESSION_DURATION); 
				}

				minsLeft = ConfConstants.SESSION_DURATION
						- AppConfPlanner.sumEvents(eventSubset);
				return findSubSetOfEvents(events, minsLeft);
			} else {
				Event nextEvent = null;
				for (Event event : events) {
					if (minsLeft == event.getDuration()) {
						nextEvent = event;
						break;
					}
				}
				if (nextEvent == null) {
					nextEvent = events.remove(0);
				}

				if (nextEvent.getDuration() <= minsLeft) {
					eventSubset.add(nextEvent);
					minsLeft = minsLeft - nextEvent.getDuration();
					return findSubSetOfEvents(events, minsLeft);
				} else {
					temp1Subset.add(nextEvent);
					return findSubSetOfEvents(events, minsLeft);
				}
			}
		} else {
			return eventSubset;
		}
	}
	
	/**
	 * populates incompleteSessions with the list of existing sessions whose
	 * duration < 180
	 * 
	 * @return List
	 */
	private List<Session> populateIncompleteSessions(List<Session> sessions){
		List<Session> incompleteSessions = new ArrayList<Session>();
		if(sessions != null && !sessions.isEmpty()){
			for(Session tempSession : sessions){
				/*if(tempSession.getDuration() < ConfConstants.SESSION_DURATION){*/
				if(tempSession.isIncomplete()){
					incompleteSessions.add(tempSession);
				}
			}
		}
		return incompleteSessions;
	}
	
	/**
	 * returns true if duration of event is less than session duration + buffer
	 * duration or else false.
	 * 
	 * @param event
	 * @param session
	 * @return boolean
	 */
	private boolean doesEventFitsInAfternoonSession(Event event, Session session){
		boolean result = false;
		
		if (((ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER) - session
				.getDuration()) > event.getDuration()) {
			result = true;
		}
		return result;
	}
	
	/**
	 * populates perfectlyTimedSessions with the list of existing sessions whose
	 * duration is exactly 180mins.
	 * 
	 * @return List<Session>
	 */
	private List<Session> populatePerfectlyTimedSessions(List<Session> sessions){
		List<Session> perfectlyTimedSessions = new ArrayList<Session>();
		if(sessions != null && !sessions.isEmpty()){
			for(Session tempSession : sessions){
				/*if(tempSession.getDuration() == ConfConstants.SESSION_DURATION){*/
				if(tempSession.isPerfect()){
					perfectlyTimedSessions.add(tempSession);
				}
			}
		}
		return perfectlyTimedSessions;
	}
		
	/**
	 * It will cut down perfectlyTimedSessions List to half. Order is not taken
	 * care.
	 * 
	 * @param perfectlyTimedSessions
	 *            -> List of all sessions must be 180. As of now, this is not
	 *            checked inside this method. Fix me.
	 * @return List<Session>
	 */
	private List<Session> removeHalfOfPerfectlyTimedSessions(
			List<Session> perfectlyTimedSessions) {
		if (perfectlyTimedSessions != null && !perfectlyTimedSessions.isEmpty()) {
			int size = perfectlyTimedSessions.size();
			if(size == 1){ // if size is only one, then try to make it an after noon session to meet the conditionn of networking event
				perfectlyTimedSessions.remove(0);
			}else if((size%2) == 0){
				for (int i = 0; i < (size / 2); i++) {
					perfectlyTimedSessions.remove(0);
				}
			}else{
				for (int i = 0; i <= (size / 2); i++) {
					perfectlyTimedSessions.remove(0);
				}
			}
			
		}
		return perfectlyTimedSessions;
	}
	
	
	/**
	 * 
	 * @param sessions
	 */
	private void createTracksAndAssignSessions(List<Session> sessions){
		
		// knock off all after noon sessions
		for(Iterator<Session> itr = sessions.iterator(); itr.hasNext();){
			Session sessionElement = itr.next();
			if(sessionElement.isDefinitelyAnAfterNoonSession()){
				sessionElement.becomeAfternoonSession();
				Track newTrack = new Track();
				newTrack.addSession(sessionElement);
				tracks.add(newTrack);
				itr.remove();
			}
		}	
		
		// knock off all sessions with duration 180
		for(Iterator<Session> itr = sessions.iterator(); itr.hasNext();){
			boolean foundSlot = false;
			Session sessionElement = itr.next();
			if(sessionElement.isPerfect()){
				if(tracks != null && !tracks.isEmpty()){
					for(Iterator<Track> trackIterator = tracks.iterator() ; trackIterator.hasNext();){
						Track existingTrack = trackIterator.next();
						if(!existingTrack.isMorningOccupied()){
							sessionElement.becomeMorningSession();
							existingTrack.addSession(sessionElement);
							itr.remove();
							foundSlot = true;
							break;
						}
					}
				}
				if(!foundSlot){
					sessionElement.becomeAfternoonSession();
					Track newTrack = new Track();
					newTrack.addSession(sessionElement);
					tracks.add(newTrack);
					itr.remove();
					/*break;*/
				}
			}
		}
		
		// knock off all incomplete sessions.. sessions with duration < 180
		for(Iterator<Session> itr = sessions.iterator(); itr.hasNext();){
			boolean foundSlot = false;
			Session session = itr.next();
			if(session.isIncomplete()){
				if(tracks != null && !tracks.isEmpty()){
					for(Iterator<Track> trackIterator = tracks.iterator() ; trackIterator.hasNext();){
						Track existingTrack = trackIterator.next();
						if(!existingTrack.isMorningOccupied()){
							session.becomeMorningSession();
							existingTrack.addSession(session);
							itr.remove();
							foundSlot = true;
							break;
						}
					}
				}
				if(!foundSlot){
					session.becomeMorningSession();
					Track newTrack = new Track();
					newTrack.addSession(session);
					tracks.add(newTrack);
					itr.remove();
					/*break;*/
				}
			}
		}
	}
	
	
	/**
	 * adds lunch and networking events to tracks
	 * @param tracks
	 */
	private void addLunchAndNetworkingEvents(List<Track> tracks){
		if(tracks!=null && !tracks.isEmpty()){
			for(Iterator<Track> itr = tracks.iterator(); itr.hasNext();){
				Track track = itr.next();
				track.addLunchSession();
				track.addNetworkEvent();
			}
		}
	}
	
	/**
	 * @throws ParseException 
	 * 
	 */
	private void addStartAndEndTimes(List<Track> tracks){
		if(tracks!=null && !tracks.isEmpty()){
			for(Track track : tracks){
				if(track.getSessions() != null && !track.getSessions().isEmpty()){
					for(Session session : track.sessions){
						session.assignStartEndTimesToEvents();
					}
				}
			}
		}
	}
}
