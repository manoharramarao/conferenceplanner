package com.twassignment.conferenceplanner;

import java.util.concurrent.atomic.AtomicInteger;

import com.twassignment.conferenceplanner.common.ConfConstants;

/**
 * Represents an event in a conference.
 * Different kinds of event includes Talk, Lunch, NetworkEvent
 * 
 * @author manohar
 * @version 1.0
 */
public abstract class Event /*implements Comparable<Event>*/{
	
	static AtomicInteger nextId = new AtomicInteger();
	
	/**
	 * Event's ID
	 */
	private int eventId;
	
	/**
	 * Event's name
	 */
	private String name;
	
	/**
	 * Event's duration
	 */
	private int duration;
	
	/**
	 * Event's startTime
	 */
	private String startTime;
	
	/**
	 * Event's endTime
	 */
	private String endTime;
	
	/**
	 * Event's short description
	 */
	private String description;
	
	/**
	 * returns event ID
	 * @return int
	 */
	public int getEventId() {
		return eventId;
	}

	/**
	 * sets event ID
	 * @param eventId
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	/**
	 * returns event's name
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets event's name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * returns event's duration
	 * @return
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * sets event's duration
	 * @param duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * returns event's start time
	 * @return
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * sets event's start time
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * returns event's end time
	 * @return
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * sets event's end time
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * returns Event's description
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets Event's description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Compares event by duration
	 * @see Comparable#compareTo(Object)
	 */
	/*@Override
	public int compareTo(Event event) {
		int result = -1;
		if(getDuration() > event.getDuration()){
			result = 1;
		}else if(getDuration() == event.getDuration()){
			result = 0;
		}
		return result;
	}*/
	
	/**
	 * pretty print of event
	 */
	@Override
	public String toString() {
		StringBuilder eventDescriptionBuilder = new StringBuilder();
		eventDescriptionBuilder.append(System.lineSeparator());
		eventDescriptionBuilder.append(getStartTime() + " ");
		eventDescriptionBuilder.append(getName() + " ");
		eventDescriptionBuilder.append(getDuration());
		return eventDescriptionBuilder.toString();
		/*return "event " + getName() + " with ID " + getEventId() + " of duration " + Integer.toString(getDuration()) + " "
				+ ConfConstants.DURATION_UNITS + " starts at " + getStartTime()
				+ " and ends at " + getEndTime() + System.lineSeparator();*/
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if(obj != null && obj instanceof Event){
			if(getEventId() == ((Event)obj).getEventId()){
				result = true;
			}
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		return (9 * this.getEventId());
	}
}
