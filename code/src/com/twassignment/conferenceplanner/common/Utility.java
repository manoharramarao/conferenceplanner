package com.twassignment.conferenceplanner.common;

import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Project's utility class
 * @author manohar
 * @version 1.0
 */
public class Utility {
	
	/**
	 * returns true if string is null or string is of length 0 after trimming
	 * spaces at the beginning and end of string or if {@link String#isEmpty()}
	 * returns true
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isEmpty(String string) {
		return (null == string || string.trim().length() == 0 || string
				.isEmpty()) ? true : false;
	}
	
	public static Logger getLogger(){
		Logger logger = LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME);
		logger.setLevel(Level.FINEST);
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.ALL);
		logger.addHandler(consoleHandler);
		logger.log(Level.FINE, "logger initialized properly");
		return logger;
	}
	
}
