package com.twassignment.conferenceplanner.common;

import java.util.Collections;
import java.util.List;

import com.twassignment.conferenceplanner.Event;

public class AppConfPlanner {
	
	/**
	 * returns sum of durations of all events in the list.
	 * @param events
	 * @return
	 */
	public static synchronized int sumEvents(List<Event> events){
		int totalDuration = 0;
		for(Event event : events){
			totalDuration += event.getDuration();
		}
		return totalDuration;
	}
	
	/**
	 * 
	 * @param events
	 */
	public static synchronized void sortEvents(List<Event> events){
		Collections.sort(events, new EventComparatorByDuration());
		/*Collections.sort(events, Collections.reverseOrder());*/
	}
}
