package com.twassignment.conferenceplanner.common;

/**
 * Project's constants
 * @author manohar
 * @version 1.0
 */
public class ConfConstants {
	
	/**
	 * units in which duration of an event measured. Set to mins
	 */
	public static final String DURATION_UNITS = "mins";
	
	// constants for lunch event
	/**
	 * lunch event's name. Set to lunch
	 */
	public static final String LUNCH_EVENT_NAME = "Lunch";
	
	/**
	 * Describes lunch event.set to
	 * "This is lunch event which lasts for an hour"
	 */
	public static final String LUNCH_EVENT_DESCRIPTION = "This is lunch event which lasts for an hour";
	
	/**
	 * lunch event's end time. Set to 1:00 PM
	 */
	public static final String LUNCH_EVENT_END_TIME = "1:00 PM";
	
	/**
	 * lunch event's start time. Set to 12:00 PM
	 */
	public static final String LUNCH_EVENT_START_TIME = "12:00 PM";
	
	/**
	 * Default lightning duration that is 5
	 */
	public static final int LIGHTNING_DURATION = 5;
	/////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Default lightning event's name. "Lightning"
	 */
	public static final String LIGHTNING_NAME = "Lightning";
	
	////////////////////////////////////////////////////////////////////////////////////////////
	// session constants
	
	/**
	 * morning session type. Set to "Morning session"
	 */
	public static final String MORNING_SESSION_NAME = "Morning session";
	
	/**
	 * afternoon session type. Set to "Afternoon session"
	 */
	public static final String AFTERNOON_SESSION_NAME = "Afternoon session";
	
	/**
	 * morning session start time. Set to "9:00 AM"
	 */
	public static final String MORNING_SESSION_START_TIME = "9:00 AM";
	
	/**
	 * morning session end time. Set to "12:00 PM"
	 */
	public static final String MORNING_SESSION_END_TIME = "12:00 PM";
	
	/**
	 * afternoon session start time. Set to "1:00 PM"
	 */
	public static final String AFTERNOON_SESSION_START_TIME = "1:00 PM";
	
	/**
	 * afternoon sessino end time. Set to "4:00 PM"
	 */
	public static final String AFTERNOON_SESSION_END_TIME = "4:00 PM";
	
	/**
	 * default session duration. Set to 180 in units {@link ConfConstants#DURATION_UNITS}
	 */
	public static final int SESSION_DURATION = 180;
	
	public static final int DURATION_BUFFER = 60;
	
	////////////////////////////////////////////////////////////////////////////
	/**
	 * lunch session name
	 */
	public static final String LUNCH_SESSION_NAME = "Lunch session";
	
	/**
	 * lunch session start time. 12:00 PM
	 */
	public static final String LUNCH_SESSION_START_TIME = "12:00 PM";
	
	/**
	 * lunch session end time. 1:00 PM
	 */
	public static final String LUNCH_SESSION_END_TIME = "1:00 PM";
	
	/**
	 * lunch session duration = 60 mins
	 */
	public static final int LUNCH_DURATION = 60;
	////////////////////////////////////////////////////////////////////////////
	
	public static final String NETWORK_EVENT_NAME = "Network event";
}
