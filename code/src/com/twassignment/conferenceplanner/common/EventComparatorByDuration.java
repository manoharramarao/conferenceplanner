package com.twassignment.conferenceplanner.common;

import java.util.Comparator;

import com.twassignment.conferenceplanner.Event;

public class EventComparatorByDuration implements Comparator<Event>{

	@Override
	public int compare(Event event1, Event event2) {
		int result = 1;
		if(event1.getDuration() > event2.getDuration()){
			result = -1;
		}else if(event1.getDuration() == event2.getDuration()){
			result = 0;
		}
		return result;
	}

}
