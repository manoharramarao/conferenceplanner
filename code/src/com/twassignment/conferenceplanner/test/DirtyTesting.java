package com.twassignment.conferenceplanner.test;

import java.util.ArrayList;
import java.util.List;

import com.twassignment.conferenceplanner.Event;
import com.twassignment.conferenceplanner.Planner;
import com.twassignment.conferenceplanner.Session;
import com.twassignment.conferenceplanner.Talk;

public class DirtyTesting {

	/**
	 * @param args
	 */
	/*public static void main(String[] args) {
		
		Logger logger = LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME);
		logger.setLevel(Level.FINEST);
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.ALL);
		logger.addHandler(consoleHandler);
		logger.log(Level.FINEST, "logger initialized properly");
		
		Planner planner = new Planner();
		
		List<Event> events = new ArrayList<Event>();
		List<Session> sessions = new ArrayList<Session>();
		
		events.add(new Talk(1, 90));
		events.add(new Talk(2, 30));
		events.add(new Talk(3, 60));
		
		events.add(new Talk(100));
		events.add(new Talk(69));
		events.add(new Talk(50));
		events.add(new Talk(20));
		events.add(new Talk(15));
		events.add(new Talk(10));
		
		events.add(new Talk(8, 90));
		events.add(new Talk(7, 45));
		events.add(new Talk(4, 40));
		events.add(new Talk(5, 30));
		events.add(new Talk(6, 15));
		events.add(new Talk(4, 10));
		events.add(new Talk(4, 03));
		
		events.add(new Talk(1, 90));
		events.add(new Talk(2, 45));
		events.add(new Talk(3, 45));
		events.add(new Talk(4, 45));
		events.add(new Talk(5, 45));
		events.add(new Talk(6, 30));
		events.add(new Talk(7, 30));
		events.add(new Talk(8, 30));
		events.add(new Talk(9, 30));
		events.add(new Talk(10, 30));
		events.add(new Talk(11, 30));
		events.add(new Talk(12, 20));
		events.add(new Talk(13, 20));
		events.add(new Talk(14, 20));
		events.add(new Talk(15, 20));
		events.add(new Talk(16, 05));
		events.add(new Talk(17, 40));
		events.add(new Talk(18, 30));
		events.add(new Talk(19, 15));
		events.add(new Talk(20, 10));
		events.add(new Talk(21, 03));
		
		
		events.add(new Talk(9, 90));
		events.add(new Talk(10, 45));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		events.add(new Talk(11, 40));
		
		
		
		events.add(new Talk(0, 60));
		events.add(new Talk(1, 45));
		events.add(new Talk(2, 30));
		events.add(new Talk(3, 45));
		events.add(new Talk(4, 45));
		events.add(new Talk(6, 05));
		events.add(new Talk(7, 60));
		events.add(new Talk(8, 45));
		events.add(new Talk(9, 30));
		events.add(new Talk(10, 30));
		events.add(new Talk(11, 45));
		events.add(new Talk(11, 60));
		events.add(new Talk(11, 60));
		events.add(new Talk(11, 45));
		events.add(new Talk(11, 30));
		events.add(new Talk(11, 30));
		events.add(new Talk(11, 60));
		events.add(new Talk(11, 30));
		events.add(new Talk(11, 30));
		
		events.add(new Talk(11, 60));
		events.add(new Talk(11, 60));
		events.add(new Talk(11, 45));
		events.add(new Talk(11, 45));
		events.add(new Talk(11, 45));
		events.add(new Talk(11, 45));
		
		events.add(new Talk(190));
		events.add(new Talk(190));
		events.add(new Talk(190));
		events.add(new Talk(230));
		events.add(new Talk(170));
		events.add(new Talk(50));
		events.add(new Talk(170));
		events.add(new Talk(50));
		events.add(new Talk(170));
		events.add(new Talk(230));
		
		
		
		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(30));
		events.add(new Talk(45));
		events.add(new Talk(45));
		events.add(new Talk(05));
		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(30));
		events.add(new Talk(30));
		events.add(new Talk(45));
		events.add(new Talk(60));
		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(30));
		events.add(new Talk(30));
		events.add(new Talk(60));
		events.add(new Talk(30));
		events.add(new Talk(30));

		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(30));
		events.add(new Talk(30));
		events.add(new Talk(60));
		events.add(new Talk(30));
		events.add(new Talk(30));
		
		
		
		

		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(30));
		events.add(new Talk(30));
		events.add(new Talk(60));
		events.add(new Talk(30));
		events.add(new Talk(30));
		
		Event event1 = new Talk(60);
		Event event2 = new Talk(60);
		Event event3 = new Talk(70);
		System.out.println("event1 ID = " + event1.getId());
		System.out.println("event2 ID = " + event2.getId());
		System.out.println("event3 ID = " + event3.getId());
		System.out.println(event1.equals(event2));
		
		events.add(new Talk(60));
		events.add(new Talk(60));
		events.add(new Talk(45));
		events.add(new Talk(45));
		events.add(new Talk(45));
		events.add(new Talk(45));
		
		
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		events.add(new Talk(179));
		
		
		
		
		for(Event event : events){
			System.out.println(event.toString());
		}
		
		Collections.sort(events, Collections.reverseOrder());
		System.out.println("after sorting");
		
		for(Event event : events){
			System.out.println(event.toString());
		}
		
		System.out.println(planner.calculateNoOfTracks(events));
		
		List<Event> resultEvents = planner.findSubSetOfEvents(events, 180);
		System.out.println(resultEvents.toString());
		
		System.out.println(planner.groupInToSessions(events,180));
		System.out.println(planner.planConference(events).toString());
		
		
		for (Event event : resultEvents) {
			System.out.println("in dirty testing class");
			System.out.println(event.toString());
		}
		sessions.addAll(planner.planConference(events));
		System.out.println(planner.planConference(events).toString());
		Session session = new Session();
		session.setDuration(190);
		System.out.println(session.isDefinitelyAnAfterNoonSession());
		
	}*/

}
