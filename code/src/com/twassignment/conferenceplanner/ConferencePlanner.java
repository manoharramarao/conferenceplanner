package com.twassignment.conferenceplanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ConferencePlanner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Enter Event Name and duration of Events. Type done when you are done with your entries");
		System.out.println("No need to enter Lunch event or Networking event. It is taken by default. " +
				"If entered explicitly, then it would be considered as a seperate event");
		List<Event> events = new ArrayList<Event>();
		while(true){
			String eventName = askEventName();
			int eventDuration;
			if(eventName.equalsIgnoreCase("lightning")){
				eventDuration = 5;
			}else{
				eventDuration = askEventDuration();
			}
			String isDone = askIfUserIsDoneWithInput();
			if(isDone.equalsIgnoreCase("yes") 
					|| isDone.equalsIgnoreCase("y")){
				Event event = null;
				if(eventName.equalsIgnoreCase("lightning")){
					event = new LightningTalk();
				}else{
					event = new Talk();
				}
				event.setName(eventName);
				event.setDuration(eventDuration);
				events.add(event);
				break;
			}else{
				Event event = null;
				if(eventName.equalsIgnoreCase("lightning")){
					event = new LightningTalk();
				}else{
					event = new Talk();
				}
				event.setName(eventName);
				event.setDuration(eventDuration);
				events.add(event); 
			}
		}
		if(events != null && !events.isEmpty()){
			Planner planner = new Planner();
			System.out.println(planner.planConference(events).toString());
		}
			
	
			
	}
	
	static String askEventName(){
		String eventName = null;
		System.out.println("Enter Event or talk Name. If it is lightning talk just say lightning");
		try{
		BufferedReader inputName = new BufferedReader(new InputStreamReader(System.in));
		eventName = inputName.readLine();
		if(eventName == null){
			System.out.println("Event name can not be null");
			askEventName();
		}
		}catch(IOException e){
			e.printStackTrace();
		}
		return eventName;
	}
	
	static int askEventDuration(){
		String eventDuration = null;
		System.out.println("Enter event duration. Should be a number.");
		try{
		BufferedReader inputName = new BufferedReader(new InputStreamReader(System.in));
		eventDuration = inputName.readLine();
		if(eventDuration == null){
			System.out.println("event duration can not be null");
			askEventDuration();
		}
		}catch(IOException e){
			e.printStackTrace();
		}
		return Integer.parseInt(eventDuration);
	}
	
	static String askIfUserIsDoneWithInput(){
		String decision = null;
		System.out.println("Are you done with entering all the events? Yes/Y or No/N");
		try{
			BufferedReader inputName = new BufferedReader(new InputStreamReader(System.in));
			decision = inputName.readLine();
			if(decision == null 
					|| (!decision.equalsIgnoreCase("yes") 
					&& !decision.equalsIgnoreCase("no")
					&& !decision.equalsIgnoreCase("y")
					&& !decision.equalsIgnoreCase("n"))){
				askIfUserIsDoneWithInput();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return decision;
	}

}
