package com.twassignment.conferenceplanner;

import java.util.List;

/**
 * Represents a talk in the conference. A talk can have multiple presenters.
 * Each session can have multiple talks
 * 
 * @author manohar
 * @version 1.0
 */
public class Talk extends Event{
	
	/**
	 * Presenter of the talk
	 */
	List<Presenter> presenters;
	
	/**
	 * A talk could be a lightning talk, in which duration of the talk would be
	 * default to 5 mins.
	 */
	boolean lightning = false;
	
	public Talk() {
		setEventId(nextId.incrementAndGet());
		/*super();*/
	}
	
	public Talk(int duration){
		this.setDuration(duration);
		setEventId(nextId.incrementAndGet());
	}
	
	/**
	 * returns all presenters of the talk
	 * @return List
	 */
	public List<Presenter> getPresenters() {
		return presenters;
	}
	
	/**
	 * sets all presenters of the talk
	 * @param presenter
	 */
	public void setPresenters(List<Presenter> presenters) {
		this.presenters = presenters;
	}
	
	/**
	 * says whether talk is lightning.
	 * @return boolean
	 */
	public boolean isLightning(){
		return lightning;
	}
	
	/**
	 * makes talk a lightning talk
	 * @param lightning
	 */
	public void isLightning(boolean lightning){
		this.lightning = lightning;
		// if talk is lightning, then set duration to 5 mins
		if(this.isLightning()){
			this.setDuration(5);
		}
	}
	
	/*@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if(obj != null && obj instanceof Talk){
			if(this.getId() == ((Talk)obj).getId()){
				result = true;
			}
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		return (9 * this.getId());
	}*/
}
