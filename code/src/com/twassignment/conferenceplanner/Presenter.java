package com.twassignment.conferenceplanner;

/**
 * Individual who is presenting talk.
 * 
 * @author manohar
 * @version 1.0
 */
public class Presenter {

	/**
	 * Presenter's name
	 */
	private String name;

	/**
	 * returns presenter's name
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets presenter's name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
