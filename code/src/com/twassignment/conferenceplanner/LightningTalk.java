package com.twassignment.conferenceplanner;

import com.twassignment.conferenceplanner.common.ConfConstants;
import com.twassignment.conferenceplanner.common.Utility;

/**
 * Kind of Talk and its name is lightning. Duration by default is 5
 * mins.
 * 
 * @see Talk
 * @author manohar
 * @version 1.0
 */
public class LightningTalk extends Talk {

	/**
	 * @see Talk#setName(String)
	 * @see ConfConstants#LIGHTNING_NAME
	 */
	@Override
	public void setName(String name) {
		if (Utility.isEmpty(name)) {
			name = ConfConstants.LIGHTNING_NAME;
		}
		super.setName(name);
	}

	/**
	 * @see Talk#setDuration(int)
	 * @see ConfConstants#LIGHTNING_DURATION
	 */
	@Override
	public void setDuration(int duration) {
		duration = ConfConstants.LIGHTNING_DURATION;
		super.setDuration(duration);
	}
}
