package com.twassignment.conferenceplanner;

import com.twassignment.conferenceplanner.common.ConfConstants;

/**
 * 
 * @author manohar
 *
 */
public class NetworkEvent extends Event {

	// TODO this type of event will have condition. It can not start before 4pm
	// and after 5pm. Code the condition
	// TODO create NetworkEvent exception
	
	public NetworkEvent() {
		setName(ConfConstants.NETWORK_EVENT_NAME);
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if(obj != null && obj instanceof Talk){
			if(this.getEventId() == ((Talk)obj).getEventId()){
				result = true;
			}
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		return (9 * this.getEventId());
	}

}
