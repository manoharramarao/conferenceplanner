package com.twassignment.conferenceplanner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents a track in conference. A track can have multiple sessions. Track
 * is identified by its name
 * 
 * @author manohar
 * @version 1.0
 */
public class Track {
	
	static AtomicInteger nextId = new AtomicInteger();
	
	public Track() {
		setId(nextId.incrementAndGet());
		/*super();*/
	}
	
	/**
	 * List of sessions in the track
	 */
	List<Session> sessions = new ArrayList<Session>();
	
	/**
	 * name of the track
	 */
	String name;
	
	/**
	 * Track ID
	 */
	int id;

	/**
	 * returns list of session in the track
	 * @return List<Sessions>
	 */
	public List<Session> getSessions() {
		return sessions;
	}

	/**
	 * sets all session for the track
	 * @param sessions
	 */
	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	/**
	 * returns name of the track
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets name of the track
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * returns track id
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * sets track id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Track does its initial job of including default sessions
	 */
	public void init(){
		includeDefaultSessions();
	}
	
	/**
	 * includes default sessions to itself
	 */
	private void includeDefaultSessions(){
		includeMorningSession();
		includeAfternoonSession();
	}
	
	/**
	 * includes morning session to itself
	 */
	private void includeMorningSession(){
		Session session = new Session();
		session.becomeMorningSession();
		this.sessions.add(session);
	}
	
	/**
	 * includes afternoon session to itself
	 */
	private void includeAfternoonSession(){
		Session session = new Session();
		session.becomeAfternoonSession();
		this.sessions.add(session);
	}
	
	@Override
	public String toString() {
		StringBuilder trackDescriptionBuilder = new StringBuilder();
		trackDescriptionBuilder.append(System.lineSeparator());
		trackDescriptionBuilder.append(System.lineSeparator());
		trackDescriptionBuilder.append("Track ID = " + getId() + ":");
		trackDescriptionBuilder.append(System.lineSeparator());
		if(sessions!= null && !sessions.isEmpty()){
			for(Session session : sessions){
				if(session.isMorningSession()){
					trackDescriptionBuilder.append(session.toString());
				}
			}
			for(Session session : sessions){
				if(session.isLunchSession()){
					trackDescriptionBuilder.append(session.toString());
				}
			}
			for(Session session : sessions){
				if(session.isAfterNoonSession()){
					trackDescriptionBuilder.append(session.toString());
				}
			}
		}
		return trackDescriptionBuilder.toString();
	}
	
	public void addSession(Session session){
		if(sessions == null || sessions.isEmpty()){
			sessions = new ArrayList<Session>();
		}
		sessions.add(session);
	}
	
	/**
	 * returns true if morning session is already occupied by this track
	 * @return
	 */
	public boolean isMorningOccupied(){
		boolean result = false;
		if(sessions != null && !sessions.isEmpty()){
			for(Session session : sessions){
				if(session.isMorningSession()){
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * adds lunch sessino to the track. If there is any existing lunch session,
	 * then it will remove that and then add newly created lunch session
	 * 
	 */
	public void addLunchSession(){
		Session lunchSession = new Session();
		lunchSession.becomeLunchSession();
		if(sessions == null || sessions.isEmpty()){
			sessions = new ArrayList<Session>();
		}
		if(sessions != null && !sessions.isEmpty()){
			for(Iterator<Session> itr = sessions.iterator(); itr.hasNext();){
				Session session = itr.next();
				if(session.isLunchSession()){
					itr.remove();
				}
			}
		}
		sessions.add(lunchSession);
	}
	
	/**
	 * adds network event to the track This will add network event only if an
	 * afternoon session exists. otherwise no error is thrown, but no network
	 * event is added to the track
	 * 
	 */
	public void addNetworkEvent(){
		Event networkEvent = new NetworkEvent();
		if(sessions != null || !sessions.isEmpty()){
			for(Iterator<Session> itr = sessions.iterator(); itr.hasNext();){
				Session session = itr.next();
				if(session.isAfterNoonSession()){
					session.addEvents(networkEvent);
				}
			}
		}
	}
}
