package com.twassignment.conferenceplanner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import com.twassignment.conferenceplanner.common.AppConfPlanner;
import com.twassignment.conferenceplanner.common.ConfConstants;
import com.twassignment.conferenceplanner.common.Utility;

/**
 * Represents a session in conference. Session starts at a given time and ends
 * at a given time. Each session is comprised of Events (Talk, Lunch,
 * lightning). Each talk is given by a presenter.
 * 
 * @see Event
 * @author manohar
 * @version 1.0
 */
public class Session {

	/**
	 * Type of session
	 */
	String name;

	/**
	 * session's start time in 24hrs.
	 */
	String startTime;

	/**
	 * session's end time
	 */
	String endTime;

	/**
	 * List of events in the session
	 */
	List<Event> events;

	/**
	 * Duration of the session in units set by {@link ConfConstants#DURATION_UNITS}
	 */
	int duration = 0;
	
	/**
	 * returns name of the session. For example - morning, afternoon, evening
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets session name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * returns session's start time
	 * 
	 * @return
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * sets session's start time
	 * 
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * return session's end time
	 * 
	 * @return
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * set session's end time
	 * 
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * Returns list of events schedules in the session
	 * 
	 * @return List
	 */
	public List<Event> getEvents() {
		return events;
	}

	/**
	 * Sets list of events scheduled in the session
	 * 
	 * @param events
	 */
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	/**
	 * adds list of events to the session
	 * @param eventsList
	 */
	public void addEvents(List<Event> eventsList){
		if(eventsList != null && !eventsList.isEmpty()){
			if(this.events == null){
				this.events = new ArrayList<Event>();
			}
			this.events.addAll(eventsList);
		}
	}
	
	/**
	 * adds an event to the session
	 * @param event
	 */
	public void addEvents(Event event){
		if(event != null){
			if(this.events == null){
				this.events = new ArrayList<Event>();
			}
			this.events.add(event);
		}
	}
	
	/**
	 * returns duration of the session in {@link ConfConstants#DURATION_UNITS}
	 * units. If the duration is zero, then it tries calculate the duration by
	 * adding duration of each event.
	 * 
	 * @return int
	 */
	public int getDuration() {
		if (events != null && !events.isEmpty()) {
			setDuration(AppConfPlanner.sumEvents(events));
		}
		return duration;
	}

	/**
	 * sets duration of the session in {@link ConfConstants#DURATION_UNITS} units
	 * @param duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Turns itself to morning session
	 */
	public void becomeMorningSession(){
		this.setName(ConfConstants.MORNING_SESSION_NAME);
		this.setStartTime(ConfConstants.MORNING_SESSION_START_TIME);
		this.setEndTime(ConfConstants.MORNING_SESSION_END_TIME);
		this.setDuration(ConfConstants.SESSION_DURATION);
	}
	
	/**
	 * Turns itself to afternoon session
	 */
	public void becomeAfternoonSession(){
		this.setName(ConfConstants.AFTERNOON_SESSION_NAME);
		this.setStartTime(ConfConstants.AFTERNOON_SESSION_START_TIME);
		this.setEndTime(ConfConstants.AFTERNOON_SESSION_END_TIME);
		this.setDuration(ConfConstants.SESSION_DURATION);
	}
	
	public void becomeLunchSession(){
		this.setName(ConfConstants.LUNCH_SESSION_NAME);
		this.setStartTime(ConfConstants.LUNCH_SESSION_START_TIME);
		this.setEndTime(ConfConstants.LUNCH_SESSION_END_TIME);
		this.setDuration(ConfConstants.LUNCH_DURATION);
	}
	
	
	@Override
	public String toString() {
		StringBuilder sessionDescriptionBuilder = new StringBuilder();
		if(isLunchSession()){
			sessionDescriptionBuilder.append(System.lineSeparator());
			sessionDescriptionBuilder.append(getName());
		}
		/*sessionDescriptionBuilder.append("session name = " + getName());
		sessionDescriptionBuilder.append(System.getProperty("line.separator"));*/
		if(this.events != null && !this.events.isEmpty()){
			/*sessionDescriptionBuilder.append("this session has " + this.events.size() + " events");
			sessionDescriptionBuilder.append(System.getProperty("line.separator"));
			sessionDescriptionBuilder.append(" list of events in this session are");
			sessionDescriptionBuilder.append(System.getProperty("line.separator"));*/
			for(Event event : events){
				sessionDescriptionBuilder.append(event.toString());
			}
			/*for(Event tempEvent : this.events){
				sessionDescriptionBuilder.append("event " + tempEvent.getName() + " for duration " + tempEvent.getDuration());
				sessionDescriptionBuilder.append(System.getProperty("line.separator"));
			}*/
		}
		return sessionDescriptionBuilder.toString();
	}
	
	public boolean isPerfect(){
		return (this.duration == 180);
	}
	
	/**
	 * if the session duration is > 180 and < 240 then it is considered as after noon session
	 * @return boolean
	 */
	public boolean isDefinitelyAnAfterNoonSession() {
		return (this.duration > ConfConstants.SESSION_DURATION && 
				this.duration <= (ConfConstants.SESSION_DURATION + ConfConstants.DURATION_BUFFER));
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isMorningSession(){
		boolean result = false;
		if(getName() != null && getName().equalsIgnoreCase(ConfConstants.MORNING_SESSION_NAME)){
			result = true;
		}
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isAfterNoonSession(){
		boolean result = false;
		if(getName() != null && getName().equalsIgnoreCase(ConfConstants.AFTERNOON_SESSION_NAME)){
			result = true;
		}
		return result;
	}
	
	/**
	 * session is incomplete if its duration < 180. Any of morning and after
	 * noon session could be incomplete session
	 * 
	 * @return
	 */
	public boolean isIncomplete(){
		return (getDuration() < 180);
	}
	
	public boolean isLunchSession(){
		boolean result = false;
		if (getName() != null
				&& getName().equalsIgnoreCase(ConfConstants.LUNCH_SESSION_NAME)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * 
	 * @throws ParseException
	 */
	public void assignStartEndTimesToEvents(){
		/* String myTime = "14:10"; */
		String sessionStartTime = null;
		try{
		SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
		Calendar time = null;
		if (isAfterNoonSession()) {
			sessionStartTime = "01:00 PM";
			Date startTime = df.parse(sessionStartTime);
			time = Calendar.getInstance();
			time.setTime(startTime);
			time.set(Calendar.AM_PM, Calendar.PM);
		} else if (isLunchSession()) {
			sessionStartTime = "12:00 PM";
			Date startTime = df.parse(sessionStartTime);
			time = Calendar.getInstance();
			time.setTime(startTime);
			time.set(Calendar.AM_PM, Calendar.PM);
		} else if (isMorningSession()) {
			sessionStartTime = "09:00 AM";
			Date startTime = df.parse(sessionStartTime);
			time = Calendar.getInstance();
			time.setTime(startTime);
			time.set(Calendar.AM_PM, Calendar.AM);
		}
		
			/* cal.add(Calendar.MINUTE, 0); */

			if (events != null && !events.isEmpty()) {
				Event networkEvent = null;
				for (Event event : events) {
					if (event instanceof NetworkEvent) {
						networkEvent = event;
					} else {
						event.setStartTime(df.format(time.getTime()));
						time.add(Calendar.MINUTE, event.getDuration());
						event.setEndTime(df.format(time.getTime()));
					}
				}
				if (networkEvent != null) {
					networkEvent.setStartTime(df.format(time.getTime()));
				}
				/*
				 * for(Iterator<Event> itr = events.iterator();itr.hasNext();){
				 * Event event = itr.next(); if(event instanceof NetworkEvent){
				 * hasNetworkEvent = true; }else{
				 * event.setStartTime(df.format(time.getTime()));
				 * time.add(Calendar.MINUTE, event.getDuration());
				 * event.setEndTime(df.format(time.getTime())); } }
				 */
			}
		}catch(ParseException e){
			Utility.getLogger().log(Level.SEVERE, "could not able to add time to events");
		}
		
	}
}
